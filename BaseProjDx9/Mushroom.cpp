#include "Mushroom.h"
#include "Engine/ResouceManager/Model.h"

const float GRAVITY = (5.0f / 60.0f);		// 重力

//コンストラクタ
Mushroom::Mushroom(IGameObject * parent) : IGameObject(parent, "Mushroom"), hModel_(-1), move_(D3DXVECTOR3(0, 0, 0)), radius_(0.5f),
	time_(0), isCollision_(false), isTime_(false)
{
}

//デストラクタ
Mushroom::~Mushroom()
{
}

//初期化
void Mushroom::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Data/Model/Block/Kinoko.fbx");

	//移動方向の初期化
	move_ = D3DXVECTOR3(6.0f / 60.0f, 0, 0);

	//ステージの取得
	pStage_ = (Stage*)GetParent()->FindChildObject("Stage");

	//コライダーをつける
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(1, 1, 1));
	AddCollider(collision);
}

//更新
void Mushroom::Update()
{
	//進行方向に移動
	_position.x += move_.x;

	_position.y -= GRAVITY;

	//土管にぶつかったら反対方向へ
	if (time_ == 0 && isCollision_)
	{
		move_.x *= -1;				//進行方向を逆方向に変更する
		isCollision_ = false;		//衝突フラグをfalseにする
		isTime_ = true;
	}
	//※判定後に移動するとおかしくなるのでWallCollision()の前で移動処理をする

	//計測を開始した場合
	if (isTime_)
	{
		time_++;		//時間を足す
	}

	//衝突判定
	WallCollision();
}

//描画
void Mushroom::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void Mushroom::Release()
{
}

//壁との衝突判定
void Mushroom::WallCollision()
{
	//チェックするマス
	int checkX;
	int checkY;

	//左のブロックをみる　
	checkX = (int)(_position.x - radius_);
	checkY = (int)_position.y;
	if (!pStage_->IsEmpty(checkX, checkY))
	{
		_position.x = checkX + 1 + radius_;
		isCollision_ = true;
	}

	//右ののブロックをみる　
	checkX = (int)(_position.x + radius_);
	checkY = (int)_position.y;
	if (!pStage_->IsEmpty(checkX, checkY))
	{
		_position.x = checkX - radius_;
		isCollision_ = true;
	}

	//下のブロックをみる　
	checkX = (int)_position.x;
	checkY = (int)(_position.y - radius_);
	if (!pStage_->IsEmpty(checkX, checkY))
	{
		_position.y = checkY + radius_ + 1;
	}

	//上のブロックをみる　
	checkX = (int)_position.x;
	checkY = (int)(_position.y + radius_);
	if (!pStage_->IsEmpty(checkX, checkY))
	{
		_position.y = checkY - radius_;
	}
}