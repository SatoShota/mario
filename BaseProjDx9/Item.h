#pragma once
#include "Engine/GameObject/GameObject.h"

//アイテムを管理するクラス
class Item : public IGameObject
{
	
public:
	//コンストラクタ
	Item(IGameObject* parent);

	//デストラクタ
	~Item();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};