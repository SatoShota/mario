#include "bootScene.h"
#include "Engine/global.h"

BootScene::BootScene(IGameObject * parent)
	: IGameObject(parent, "BootScene"), _pText(nullptr)
{
}

void BootScene::Initialize()
{
	//テキストを作成
	_pText = new Text("ＭＳ ゴシック", 32);
}

void BootScene::Update()
{
}

void BootScene::Draw()
{
	_pText->Draw(100, 100, "これはテスト用のシーンです。");
}

void BootScene::Release()
{
	delete _pText;
}
