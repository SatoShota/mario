#pragma once
#include "Engine/GameObject/GameObject.h"
#include "Stage.h"

//プレイヤーを管理するクラス

const float DEFAULT_SPEED = (2.0f / 60.0f);

//◆◆◆を管理するクラス

class Player : public IGameObject
{
	//モデルの識別番号
	int hModel;
	int hBigModel_;			// でかいマリオ

	//動き
	D3DXVECTOR3 move_;

	//走る
	D3DXVECTOR3 dash_;

	//ジャンプ
	D3DXVECTOR3 jamp_;

	//ステージの配列
	Stage* pStage_;

	//マリオの半径
	float radius_;

	//きのこをとったか否か
	bool isGetKinoko_;

public:
	//コンストラクタ
	Player(IGameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	void WallCollision();

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//衝突処理
	void OnCollision(IGameObject* pTarget);
	//
	D3DXVECTOR3 GetDash();

};
