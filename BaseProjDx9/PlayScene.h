#pragma once

#include "Engine/global.h"
#include "Engine/GameObject/Camera.h"

//プレイシーンを管理するクラス

#include "Player.h"



//■■シーンを管理するクラス

class PlayScene : public IGameObject
{
	Camera* pCamera;

	D3DXVECTOR3 cameraPos_;
	
	Player* pPlayer;
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//きのこを出す
	void CreateKinoko(int x, int y);
};