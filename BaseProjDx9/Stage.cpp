#include "Stage.h"
#include <string>
#include "Engine/ResouceManager/CsvReader.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Stage::Stage(IGameObject * parent)
	:IGameObject(parent, "Stage")
{
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	std::vector<std::string> vecFileName;
	GetFileName(vecFileName, "Data/map.txt");

	CsvReader csv;
	csv.Load("data/Map1-1.csv");

	for (int i = 0; i < 14; i++)
	{
		for (int j = 0; j < 200; j++)
		{
			map_[i][j] = csv.GetValue(j, 14 - i);
		}
	}

	for (int i = 0; i < GOAL; i++)
	{
		hModel_[i] = Model::Load(vecFileName.at(i));
	}
	
}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
	//マップ描画
	for (int i = 0; i < 14; i++)
	{
		for (int j = 0; j < 200; j++)
		{
			D3DXMATRIX m;
			D3DXMatrixTranslation(&m, j + 0.5, i + 1.5 , 0);

			if (map_[i][j] != 0)
			{
				Model::SetMatrix(hModel_[map_[i][j]],m );
				Model::Draw(hModel_[map_[i][j]]);
			}
			
		}
	}

}

//開放
void Stage::Release()
{
}

void Stage::GetFileName(std::vector<std::string>& vecFileName, std::string fileName)
{
	// ファイルを開く
		HANDLE hFile;
	hFile = CreateFile(fileName.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	//ファイルのサイズ（文字数）を調べる
	DWORD fileSize = GetFileSize(hFile, NULL);

	//すべての文字を入れられる配列を用意
	char* data;
	data = new char[fileSize];

	//ファイルの中身を配列に読み込む
	DWORD dwBytes = 0;
	ReadFile(hFile, data, fileSize, &dwBytes, NULL);

	//開いたファイルを閉じる
	CloseHandle(hFile);

	std::string fbxName;

	int i = 0;

	while (i < fileSize)
	{
		//特定文字がきたら改行コードがくるまでi++
		if (data[i] == '/')
		{
			while (data[i] != '\r')
			{
				i++;
			}
		}
		//データがきたらいつも通りの処理
		else
		{
			int j = 0;

			while (data[i + j] != '\r')
			{
				j++;
			}

			std::string fbxFileName(data, i, j);

			vecFileName.push_back(fbxFileName);
			fbxFileName.clear();

			i += j;
		}

		// /nと/rの分をスキップ
		i += 2;

	}

	delete data;
}

bool Stage::IsEmpty(int x, int y)
{
	if (y < 0 || x < 0)
	{
		return false;
	}

	if (map_[y][x] == EMPTY)
	{
		return true;
	}

	return false;
}

//ブロックの種類を調べる
BLOCK_TYPE Stage::IsBlock(int x, int y)
{
	switch (map_[y][x])
	{
		case HATENA:
			return HATENA;
			break;
	}
}
