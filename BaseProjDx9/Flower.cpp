#include "Flower.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Flower::Flower(IGameObject * parent) : IGameObject(parent, "Flower"), hModel_(-1)
{
}

//デストラクタ
Flower::~Flower()
{
}

//初期化
void Flower::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Data/Model/Block/fire_flower.fbx");
}

//更新
void Flower::Update()
{
}

//描画
void Flower::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void Flower::Release()
{
}