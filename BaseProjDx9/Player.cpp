#include "Player.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/gameObject/Camera.h"
#include "PlayScene.h"



//コンストラクタ
Player::Player(IGameObject * parent) : IGameObject(parent, "Player"), radius_(0.5f), move_(D3DXVECTOR3(0,0,0)), jamp_(D3DXVECTOR3(0, 0, 0)),
	hBigModel_(-1), isGetKinoko_(false)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//初期マリオのモデルをロード
	hModel = Model::Load("Data/testmario.fbx");
	
	//Camera* pCamera = CreateGameObject<Camera>(this);

	//でかマリオのモデルをロード
	hBigModel_ = Model::Load("Data/Bigmario.fbx");

	////カメラの初期設定
	//pCamera->SetPosition(D3DXVECTOR3(0, 4, -10));
	//pCamera->SetTarget(D3DXVECTOR3(0, 4, 0));*/
	
	//初期位置の設定
	_position.y = 5;
	_position.z = 1;
	_position.x = 8;

	//ステージのポインタ取得
	pStage_ = (Stage*)GetParent()->FindChildObject("Stage");

	//移動ベクトルの初期設定
	move_ = (D3DXVECTOR3(DEFAULT_SPEED, 0, 0));

	//コライダーをつける
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 0, -1), D3DXVECTOR3(1, 1, 1));
	AddCollider(collision);
}

//更新
void Player::Update()
{
	if (Input::IsKey(DIK_LSHIFT) && dash_.x < DEFAULT_SPEED * 6)
	{
		dash_.x += DEFAULT_SPEED * 2;
	}

	//1秒あたりにブロックすすむ
	if (Input::IsKey(DIK_A))
	{
		_position.x -= move_.x + dash_.x;
	}
	if (Input::IsKey(DIK_D))
	{
		_position.x += move_.x + dash_.x;
	}
	if (Input::IsKey(DIK_SPACE))
	{
		_position.y += DEFAULT_SPEED * 10;
	}

	//摩擦（スピードダウン）
	if (dash_.x > 0)
	{
		dash_.x -= DEFAULT_SPEED / 2;
	}
	
	//重力
	_position.y -= DEFAULT_SPEED * 7;

	WallCollision();
}

void Player::WallCollision()
{
	//チェックするマス
	int checkX;
	int checkY;

	//左のブロックをみる　
	checkX = (int)(_position.x - radius_);
	checkY = (int)_position.y;
	if (!pStage_->IsEmpty(checkX, checkY))
	{
		_position.x = checkX + 1 + radius_;
	}

	//右ののブロックをみる　
	checkX = (int)(_position.x + radius_);
	checkY = (int)_position.y;
	if (!pStage_->IsEmpty(checkX, checkY))
	{
		_position.x = checkX - radius_;
	}

	//下のブロックをみる　
	checkX = (int)_position.x;
	checkY = (int)(_position.y - radius_);
	if (!pStage_->IsEmpty(checkX, checkY))
	{
		_position.y = checkY + radius_ + 1;
	}

	//上のブロックをみる　
	checkX = (int)_position.x;
	checkY = (int)(_position.y + radius_);

	if (!pStage_->IsEmpty(checkX, checkY))
	{
		_position.y = checkY - radius_;

		//衝突したブロックがハテナブロックだった場合、きのこを出現させる
		if (pStage_->IsBlock(checkX, checkY) == HATENA)
		{
			PlayScene* pPlayScene = (PlayScene*)GetParent();	//親をとってくる
			pPlayScene->CreateKinoko(checkX, checkY);			//衝突したブロックの2上にきのこを出す
		}
	}
}

//描画
void Player::Draw()
{
	//D3DXMATRIX m;
	//D3DXMatrixTranslation(&m, 0, -12, 0);

	//きのこをとっていた場合
	if (isGetKinoko_)
	{
		//でかいマリオを描画する
		Model::SetMatrix(hBigModel_, _worldMatrix);
		Model::Draw(hBigModel_);
	}
	else
	{
		//初期マリオを描画する
		Model::SetMatrix(hModel, _worldMatrix);
		Model::Draw(hModel);
	}
}

//開放
void Player::Release()
{
}


//衝突応答
void Player::OnCollision(IGameObject * pTarget)
{
	isGetKinoko_ = true;
	pTarget->KillMe();
}

D3DXVECTOR3 Player::GetDash()
{
	return dash_;
}

