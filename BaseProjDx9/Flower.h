#pragma once
#include "Engine/GameObject/GameObject.h"

//ファイアーフラワーを管理するクラス
class Flower : public IGameObject
{
	int hModel_;		//ファイアーフラワーのモデル番号

public:
	//コンストラクタ
	Flower(IGameObject* parent);

	//デストラクタ
	~Flower();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};