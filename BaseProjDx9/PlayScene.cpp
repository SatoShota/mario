#include "PlayScene.h"
#include "Stage.h"

#include "Player.h"
#include "Mushroom.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"),cameraPos_(D3DXVECTOR3(0,0,0))
{
}

//初期化
void PlayScene::Initialize()
{
	pCamera = CreateGameObject<Camera>(this);


	CreateGameObject<Stage>(this);

	CreateGameObject<Player>(this);

	CreateGameObject<Mushroom>(this);

	//
	//pCamera->SetPosition(D3DXVECTOR3(0, 5, -10));
	//pCamera->SetTarget(D3DXVECTOR3(0, 0, 0));

	pPlayer = CreateGameObject<Player>(this);


	
	pCamera->SetPosition(D3DXVECTOR3(pPlayer->GetPosition().x, 7.5, -10));
	pCamera->SetTarget(D3DXVECTOR3(pPlayer->GetPosition().x, 7.5, 0));

	cameraPos_.x = pPlayer->GetPosition().x;
}

//更新
void PlayScene::Update()
{
	//


	//1秒あたりにブロックすすむ
	if (Input::IsKey(DIK_D))
	{
		if (cameraPos_.x <= pPlayer->GetPosition().x)
		{
			cameraPos_.x = pPlayer->GetPosition().x;
			pCamera->SetPosition(D3DXVECTOR3((cameraPos_.x), 7.5, -10));
			pCamera->SetTarget(D3DXVECTOR3((cameraPos_.x), 7.5, 0));
		}
	}
	/*if (Input::IsKey(DIK_D))
	{
		
		cameraPos_.x = pPlayer->GetPosition().x;
		pCamera->SetPosition(D3DXVECTOR3((cameraPos_.x),7, -10));
		pCamera->SetTarget(D3DXVECTOR3((cameraPos_.x),7, 0));
	}*/
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}

//きのこを出す
void PlayScene::CreateKinoko(int x, int y)
{
	CreateGameObject<Mushroom>(this)->SetPosition(D3DXVECTOR3(x, y + 2, 0));
}
