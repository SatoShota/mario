#pragma once
#include "Engine/GameObject/GameObject.h"
#include <vector>

enum BLOCK_TYPE
{
	EMPTY,
	GROUND,
	BRICK,
	STAIRS,
	HATENA,
	PIPE_HEADLEFT,
	PIPE_HEADRIGHT,
	PIPE_LEFT,
	PIPE_RIGHT,
	KINOKO,
	COIN,
	GOAL,
	MAP_MAX
};

//◆◆◆を管理するクラス
class Stage : public IGameObject
{
	//CSVも読み込む
	int map_[14][200];

	//モデルの識別番号
	int hModel_[MAP_MAX];

public:
	//コンストラクタ
	Stage(IGameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void GetFileName(std::vector<std::string> &vecFileName, std::string fileName);

	bool IsEmpty(int x, int y);

	//ブロックの種類を調べる
	BLOCK_TYPE IsBlock(int x, int y);
};