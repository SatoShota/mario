#pragma once
#include "Stage.h"
#include "Engine/GameObject/GameObject.h"

//アイテムを管理するクラス
class Mushroom : public IGameObject
{
	Stage* pStage_;		//ステージ

	int hModel_;		//きのこのモデル番号

	D3DXVECTOR3 move_;	//移動方向

	float radius_;		//きのこの半径

	int time_;			//時間

	bool isCollision_;	//衝突したか否か
	bool isTime_;		//時間を計測するか否か

public:
	//コンストラクタ
	Mushroom(IGameObject* parent);

	//デストラクタ
	~Mushroom();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
	void WallCollision();
};